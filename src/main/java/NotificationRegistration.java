import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.io.*;
import java.sql.*;

public class NotificationRegistration {
    public void RegisterNotification(Notification notification)
    {
        SqlConnectData sqlConnectData = new SqlConnectData();
        Connection connection = null;
        Statement statement = null;
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(sqlConnectData.getURL(),sqlConnectData.getUSERNAME(),
                    sqlConnectData.getPASSWORD());
            statement = connection.createStatement();
            statement.execute("INSERT INTO notifications(user_id, notification, date, time) VALUES ("
                    + notification.getUserID()
            + ",'" + notification.getNotification() + "', '" + notification.getDate() + "', '"
                    + notification.getTime() +"');" );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
