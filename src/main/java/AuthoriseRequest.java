import java.io.*;
import java.net.Socket;


public class AuthoriseRequest implements Request,Serializable {
    private String login;
    private String password;
    private String id;

    public AuthoriseRequest(String login, String password) {
        this.login = login;
        this.password = password;

    }

    public String getId() {
        return id;
    }

    @Override
    public void doRequest(Socket socket) {
        UserFinder userFinder = new UserFinder(login,password);
        userFinder.DetectID();
        id = userFinder.getUserID();
        PrintWriter outputStream = null;
        try {
            outputStream = new PrintWriter(socket.getOutputStream());
            outputStream.println(id);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
