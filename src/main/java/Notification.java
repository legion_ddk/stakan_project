
public class Notification {
    private String notification;
    private String date;
    private String time;
    private String userID;
    Notification(String userID, String notification, String date, String time)
    {
        this.userID = userID;
        this.notification = notification;
        this.date = date;
        this.time = time;
    }
    @Override
    public boolean equals(Object obj) {
        Notification notification = (Notification) obj;
        if (this.userID.equals(notification.getUserID()) && this.date.equals(notification.getDate()) &&
                this.notification.equals(notification.getNotification()) && this.time.equals(notification.getTime())){
            return true;
        }
        return false;
    }
    public String getNotification() {
        return notification;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getUserID() {
        return userID;
    }
}
