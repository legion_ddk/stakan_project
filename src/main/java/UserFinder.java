import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;

public class UserFinder {
    private String login;
    private String password;
    private String userID;
    public UserFinder(String login, String password)
    {
        this.login = login;
        this.password = password;
        DetectID();
    }

    public String getUserID() {
        return userID;
    }

    public void DetectID()
    {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            SqlConnectData sqlConnectData = new SqlConnectData();
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(sqlConnectData.getURL(),sqlConnectData.getUSERNAME(),
                    sqlConnectData.getPASSWORD());
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT id_users FROM users WHERE login = '" + login +
                    "' AND password = '" + password + "';");
            resultSet.next();
            userID = resultSet.getString("id_users");
        }  catch (SQLException e) {
            userID = new String("Not Found");
        }
        finally {
            try {
                resultSet.close();
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

}