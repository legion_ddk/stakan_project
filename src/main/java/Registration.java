import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Registration {
    private JTextField IDField;
    private JTextField IDFromVKTextField;
    private JButton GenerationButton;
    private JButton Exit;
    public JPanel Registration;

    public Registration() {
        Exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { System.exit(0); }
        });
        GenerationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String Id = IDField.getText();
                Generation GEN = new Generation(Id);
                String log = GEN.getPASSWORD();
                String pas = GEN.getUSERNAME();
                JOptionPane.showMessageDialog(null, "Ваш логин : " + log + "\nВаш пароль : " + pas);
                Client User = new Client(log, pas);
                User.RegisterUser(Id, Id);
            }
        });
    }
}
