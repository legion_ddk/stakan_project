import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Queue;

public class Server {
    private final int PORT = 1234;
    private Queue<Request> requests = new ArrayDeque<>();
    private Queue<Socket> sockets = new ArrayDeque<>();
    public Server(){
        ServerSocket serverSocket = null;
        Socket socket = null;
        try{
            new Thread(() -> {
                while (true){
                    try {

                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (!requests.isEmpty()){
                        Request request = requests.remove();
                        Socket socket1 = sockets.remove();
                        request.doRequest(socket1);
                    }
                }
            }).start();
            Thread.sleep(100);
            new Thread(() -> {
                VkBot vkBot = new VkBot();
                vkBot.DeliverNotifications();
            }).start();
            serverSocket = new ServerSocket(PORT);
            while (true){
                socket = serverSocket.accept();
                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                sockets.add(socket);
                try {
                    Request request = (Request) inputStream.readObject();
                    requests.add(request);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
