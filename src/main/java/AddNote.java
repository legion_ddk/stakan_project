import javax.swing.*;
import javax.xml.bind.SchemaOutputResolver;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class AddNote {

    private JButton Add;
    public JPanel AddNote;
    protected JTextField YearField;
    private JTextField MounthField;
    private JTextField DayField;
    private JTextField HourField;
    private JTextField MinuteField;
    private JTextField NoteField;
    private JTextField гдTextField;
    private JTextField мсTextField;
    private JTextField днTextField;
    private JTextField чсTextField;
    private JTextField мнTextField;
    protected static String Year;
    protected static String Mounth;
    protected static String Day;
    protected static String Hour;
    protected static String Minute;
    protected static String Note;
    public String Time;
    public String Date;
    protected int CheckTime;
    protected int CheckDay;
    public static boolean fillingIn = false;


    public AddNote() {
        Add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Year = YearField.getText();
                Mounth = MounthField.getText();
                Day = DayField.getText();
                Hour = HourField.getText();
                Minute = MinuteField.getText();

                CheckTime = Integer.parseInt(Year);
                if ((CheckTime < 17) || (CheckTime > 99))
                {
                    JOptionPane.showMessageDialog(null, "Некорректно введён год!");
                    YearField.setText(" ");
                }
                else
                {
                    CheckTime = Integer.parseInt(Mounth);
                    if (CheckTime > 12)
                    {
                        JOptionPane.showMessageDialog(null, "Некорректно введён месяц!");
                        MounthField.setText(" ");
                    }
                    else
                    {
                        CheckDay = Integer.parseInt(Day);
                        switch (CheckTime)
                        {
                            case 1:
                                if (CheckDay > 30)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 2:
                                if (CheckDay > 28)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 3:
                                if (CheckDay > 31)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 4:
                                if (CheckDay > 30)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 5:
                                if (CheckDay > 31)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 6:
                                if (CheckDay > 30)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 7:
                                if (CheckDay > 31)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 8:
                                if (CheckDay > 31)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 9:
                                if (CheckDay > 30)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 10:
                                if (CheckDay > 31)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 11:
                                if (CheckDay > 30)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                            case 12:
                                if (CheckDay > 31)
                                {
                                    JOptionPane.showMessageDialog(null, "Некорректно введён день!");
                                    DayField.setText(" ");
                                }
                                break;
                        }
                        CheckTime = Integer.parseInt(Hour);
                        if (CheckTime > 24)
                        {
                            JOptionPane.showMessageDialog(null, "Некорректно введены часы!");
                            HourField.setText(" ");
                        }
                        CheckTime = Integer.parseInt(Minute);
                        if (CheckTime > 59)
                        {
                            JOptionPane.showMessageDialog(null, "Некорректно введены минуты!");
                            MinuteField.setText(" ");
                        }
                    }
                }
                Year = "20" + YearField.getText();
                Mounth = MounthField.getText();
                Day = DayField.getText();
                Hour = HourField.getText();
                Minute = MinuteField.getText();
                Note = NoteField.getText();
                Time = Hour + ":" + Minute;
                Date = Day + "." + Mounth + "." + Year;
                fillingIn = true;
                Client User = new Client(Login.Log, Login.Pas);
                User.setId(Login.Id);
                User.RegisterNotification(Note,Date,Time);

            }
        });
    }
}
