import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;

public class RegistrationRequest implements Request,Serializable {
    private String login;
    private String password;
    private String username;
    private String id;

    public RegistrationRequest(String login, String password, String username, String id) {
        this.login = login;
        this.password = password;
        this.username = username;
        this.id = id;
    }

    @Override
    public void doRequest(Socket socket) {
        try {
            UsersRegistration usersRegistration = new UsersRegistration();
            UnRegisteredUser user = new UnRegisteredUser(username,id,login,password);
            usersRegistration.RegisterUser(user);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
