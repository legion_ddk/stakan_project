import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;
import java.util.LinkedList;

public class TodayNotifications {
    private LinkedList<Notification> notifications = new LinkedList<>();
    private String date;
    public LinkedList<Notification> getTodayNotifications(String date) {
        this.date = date;
        FindTodayNotifications();
        return notifications;
    }

    private void FindTodayNotifications()
    {
        SqlConnectData sqlConnectData = new SqlConnectData();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            connection = DriverManager.getConnection(sqlConnectData.getURL(),
                    sqlConnectData.getUSERNAME(),
                    sqlConnectData.getPASSWORD());
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM notifications WHERE date = '" + date + "';");
            while (resultSet.next())
            {
                Notification notification = new Notification(resultSet.getString("user_id"),
                        resultSet.getString("notification"),resultSet.getString("date"),
                        resultSet.getString("time"));
                notifications.add(notification);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {

                resultSet.close();
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
