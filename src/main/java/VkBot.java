import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

public class VkBot {
    private int groupId;
    private String access_token;
    private GroupActor actor;
    private TransportClient transportClient;
    private VkApiClient vk;
    private TodayNotifications todayNotifications;
    private LinkedList<Notification> notifications;
    private ArrayList<Notification> deliveredNotifications;
    private Date date;
    private SimpleDateFormat simpleDateFormat;
    private String EtalonDate;
    public VkBot(){
        date = new Date();
        simpleDateFormat = new SimpleDateFormat("dd.M.yyyy");
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                    new FileInputStream("D:\\IdeaProjects\\ProjectLogPass\\VkInitialise.txt")));
            groupId = Integer.parseInt(bufferedReader.readLine());
            access_token = bufferedReader.readLine();
            actor = new GroupActor(groupId, access_token);
            transportClient = HttpTransportClient.getInstance();
            vk = new VkApiClient(transportClient);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        todayNotifications = new TodayNotifications();
        notifications = todayNotifications.getTodayNotifications(simpleDateFormat.format(date));
        deliveredNotifications = new ArrayList<>();
        EtalonDate = simpleDateFormat.format(date);
    }
    private void updateNotifications(){
        this.date = new Date();

        this.simpleDateFormat = new SimpleDateFormat("dd.M.yyyy");
        todayNotifications = new TodayNotifications();
        LinkedList<Notification> newNotifications = todayNotifications.getTodayNotifications(this.simpleDateFormat.format(this.date));
        for (int i = 0; i < notifications.size(); i++){
            for (int j = 0; j < newNotifications.size(); j++){
                if (notifications.get(i).equals(newNotifications.get(j))){
                    newNotifications.remove(j);
                    i=0;
                    j=0;
                }
            }
        }
        for (int i = 0; i < newNotifications.size(); i++){
            notifications.add(newNotifications.get(i));
        }
    }
    private boolean wasSent(Notification notification){

        for (int i = 0; i < deliveredNotifications.size();i++){
            if (notification.equals(deliveredNotifications.get(i))){
                return true;
            }
        }
        return false;
    }
    private boolean checkOnRain(){
        WeatherParser weatherParser = new WeatherParser();
        weatherParser.WeatherParse();
        ArrayList<Weather> weathers = weatherParser.getWeathers();
        for (int i = 0; i < weathers.size(); i++){
            if (weathers.get(i).getWeather().contains("дождь") || weathers.get(i).getWeather().contains("Дождь"))
            return true;
        }
        return false;
    }
    public void DeliverNotifications(){
        Date currentDate = new Date();
        SimpleDateFormat currentDateFormat = new SimpleDateFormat("dd.M.yyyy");
        String StrCurrentDate = currentDateFormat.format(currentDate);
        while (true){
            while (StrCurrentDate.equals(EtalonDate)){
                Date currentTime = new Date();
                SimpleDateFormat time_format = new SimpleDateFormat("H:mm");
                for (int i = 0; i < notifications.size();i++){
                    Notification notification = notifications.get(i);
                    if (notification.getTime().equals(time_format.format(currentTime)) && !wasSent(notification)){
                        try {
                            com.vk.api.sdk.actions.Messages messages = new com.vk.api.sdk.actions.Messages(vk);
                            if (!checkOnRain())
                            messages.send(actor).message(notification.getNotification()).userId(Integer.parseInt(notification.getUserID())).execute();
                            else {
                                String message = notification.getNotification() + " Будет дождь - возьмите зонт";
                                messages.send(actor).message(message).userId(Integer.parseInt(notification.getUserID())).execute();
                            }
                            Thread.sleep(100);
                        } catch (ApiException e) {
                            e.printStackTrace();
                        } catch (ClientException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        deliveredNotifications.add(notification);
                        break;
                    }
                    updateNotifications();
                }
                currentDate = new Date();
                StrCurrentDate = currentDateFormat.format(currentDate);
            }
            date = new Date();
            simpleDateFormat = new SimpleDateFormat("dd.M.yyyy");
            todayNotifications = new TodayNotifications();
            notifications = todayNotifications.getTodayNotifications(simpleDateFormat.format(date));
            deliveredNotifications = new ArrayList<>();
            EtalonDate = simpleDateFormat.format(date);

        }
    }
}