import java.awt.*;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private String id = new String();
    private String login;
    private String username;
    private String password;
    private String date;
    private String time;
    private String notification;
    private final int PORT = 1234;
    private final String HOST = "localhost";
    private int flag;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Client(String login, String password) {
        this.login = login;
        this.password = password;
    }


    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }
    public void AuthoriseUser() {
        Socket socket = null;
        try {
            socket = new Socket(HOST, PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream outputStream = null;
        Scanner inputStream = null;
        try {
            outputStream = socket.getOutputStream();
            AuthoriseRequest authoriseRequest = new AuthoriseRequest(login, password);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(authoriseRequest);
            objectOutputStream.flush();
            inputStream = new Scanner(socket.getInputStream());
            while (true) {
                if (inputStream.hasNext()) {
                    id = inputStream.nextLine();
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inputStream.close();
                outputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public void RegisterUser(String id, String username){
        this.id = id;
        this.username = username;
        RegistrationRequest registrationRequest = new RegistrationRequest(login,password,username,id);
        Socket socket = null;
        try {
            socket = new Socket(HOST, PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream outputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
            outputStream = socket.getOutputStream();
            objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(registrationRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                objectOutputStream.close();
                outputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    public void RegisterNotification(String notification, String date, String time){
        this.notification = notification;
        this.date = date;
        this.time = time;
        WriteNotificationRequest writeNotificationRequest = new WriteNotificationRequest(id,date,time,notification);
        Socket socket = null;
        try {
            socket = new Socket(HOST, PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream outputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
            outputStream = socket.getOutputStream();
            objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(writeNotificationRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                objectOutputStream.close();
                outputStream.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
