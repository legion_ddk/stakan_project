import java.io.Serializable;
import java.net.Socket;

public class WriteNotificationRequest implements Request,Serializable {
    private String id;
    private String date;
    private String time;
    private String notification;

    public WriteNotificationRequest(String id, String date, String time, String notification) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.notification = notification;
    }

    @Override
    public void doRequest(Socket socket) {
        Notification notification = new Notification(id,this.notification,date,time);
        NotificationRegistration notificationRegistration = new NotificationRegistration();
        notificationRegistration.RegisterNotification(notification);
    }
}
