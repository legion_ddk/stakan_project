import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;



public class WeatherParser {
    private ArrayList<String> str_weathers;
    private ArrayList<Weather> weathers;
    public WeatherParser()
    {
        str_weathers = new ArrayList<String>(4);
        weathers = new ArrayList<>(4);
    }

    public ArrayList<Weather> getWeathers() {
        return weathers;
    }

    public void WeatherParse()
    {

        try {

            Document doc = Jsoup.connect("https://yandex.ru/pogoda/moscow/details").get();
            Elements elements = doc.getElementsByAttributeValue("class", "weather-table__row");
            for (int i = 0; i < 4; i++) {
                Element element = elements.get(i);
                str_weathers.add(element.text());

            }
            for (int i = 0; i < 4; i++){
                String str = str_weathers.remove(i);
                for (int j = 0; j < 4; j++){
                    int index = str.lastIndexOf(" ");
                    str = str.substring(0,index);
                }
                str_weathers.add(i,str);
            }
            for (int i = 0; i < 4; i++){
                Weather weather = new Weather();
                int index = str_weathers.get(i).lastIndexOf("°");
                weather.setWeather(str_weathers.get(i).substring(index+2));
                weather.setDay_part_and_degree(str_weathers.get(i).substring(0,index+1));
                weathers.add(weather);
            }

        } catch (IOException e) {
            System.out.println("Can`t connect");
        }


    }
}