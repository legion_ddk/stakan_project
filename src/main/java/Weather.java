
public class Weather {
    private String day_part;
    private String weather = new String();

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getDay_part_and_degree() {
        return day_part;
    }

    public void setDay_part_and_degree(String day_part) {
        this.day_part = day_part;
    }
}
