import javax.swing.*;
import java.awt.*;
import java.io.IOException;


public class Main {
    public static void main(String... args) throws Exception {



        JFrame Frame = new JFrame ("Log in");
        Frame.setSize(new Dimension(350, 120));
        Frame.setLocationRelativeTo(null);
        Frame.setContentPane(new Login().Login);
        Frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Frame.setUndecorated(false);
        Frame.setVisible(true);
        Frame.setResizable(false);


        Offline OFF = new Offline();
        OFF.DeleteOldNotes();
        OFF.SelectNote();

        new Thread()
        {
            public void run()
            {
                boolean Run = true;
                while (Run)
                {
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (Login.WinAuto)
                    {
                        Frame.dispose();
                        Login.WinAuto = false;
                    }

                    if(AddNote.fillingIn){
                        try {
                            OFF.CreateNewNote(AddNote.Note, AddNote.Day, AddNote.Mounth, AddNote.Year, AddNote.Hour, AddNote.Minute);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        AddNote.fillingIn = false;
                    }
                }
            }
        }.start();
    }
}
