import com.mysql.fabric.jdbc.FabricMySQLDriver;


import java.io.*;
import java.sql.*;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.Statement;

public class UsersRegistration {
    public UsersRegistration() throws ClassNotFoundException, IOException {

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void RegisterUser(UnRegisteredUser unRegisteredUser) {
        Statement statement = null;
        Connection connection = null;
        SqlConnectData sqlConnectData = new SqlConnectData();
        try {
            connection = DriverManager.getConnection(sqlConnectData.getURL(),sqlConnectData.getUSERNAME(),
                    sqlConnectData.getPASSWORD());
            statement = connection.createStatement();
            statement.execute("INSERT INTO users(id_users, username, login, password) VALUES ( " +
                    unRegisteredUser.getUserId() + ", '" + unRegisteredUser.getUsername() + "', '" +
                    unRegisteredUser.getUserLogin() +
                    "', '" + unRegisteredUser.getUserPassword() + "');");

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}