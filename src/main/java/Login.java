import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Login {
    public JPanel Login;
    private JTextField LoginTextField;
    public JTextField LoginField;
    private JTextField PasswordTextField;
    public JPasswordField PasswordField;
    private JButton Autorisation;
    private JButton Registration;
    public static String Log;
    public static String Pas;
    public static String Id;
    public static boolean WinAuto = false;

    public Login() {
        Autorisation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                    Log = LoginField.getText();
                    Pas = PasswordField.getText();
                    Client User = new Client(Log, Pas);
                    User.AuthoriseUser();
                    Id = User.getId();

                    if (Id.equals("Not Found")) {
                        JOptionPane.showMessageDialog(null, "Некорректный логин и пароль!");
                    }
                    else {
                        JFrame FrameMain = new JFrame("App");    // Создание окна с диалогом.
                        FrameMain.setSize(new Dimension(370, 200));    // Размер окна.
                        FrameMain.setLocationRelativeTo(null);    // Обозначает местоположение.
                        FrameMain.setContentPane(new PanelMain().PanelMain);    // Берет основу для окна.
                        FrameMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // Поведение в случае того или иного события.
                        FrameMain.setUndecorated(true);    // Данная функция убирает панель свертывания, закрытия и тд.
                        FrameMain.setVisible(true);    // Видимость окна.
                        FrameMain.setResizable(false);
                        WinAuto = true;
                    }
            }
        });
        Registration.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                JFrame Frame = new JFrame ("Registration");
                Frame.setSize(new Dimension(350, 120));
                Frame.setLocationRelativeTo(null);
                Frame.setContentPane(new Registration().Registration);
                Frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                Frame.setUndecorated(false);
                Frame.setVisible(true);
                Frame.setResizable(false);
            }
        });
    }
}
