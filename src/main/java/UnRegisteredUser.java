
public class UnRegisteredUser {
    private String Username;
    private String userId;
    private String UserLogin;
    private String UserPassword;
    public UnRegisteredUser(String username, String userId, String userLogin, String userPassword)
    {
        this.Username=username;
        this.userId = userId;
        this.UserLogin = userLogin;
        this.UserPassword = userPassword;
    }
    public String getUsername() {
        return Username;
    }



    public String getUserId() {
        return userId;
    }



    public String getUserLogin() {
        return UserLogin;
    }



    public String getUserPassword() {
        return UserPassword;
    }

}
