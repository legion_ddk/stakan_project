import javax.swing.*;
import java.util.*;
import java.io.*;
import java.text.*;
import java.lang.*;
import java.nio.charset.StandardCharsets;
import java.util.Timer;

public class Offline {

    private String path = "src/StakanPrjctOffline//";
    private File dir = new File(path);
    private File[] notes;
    public String note;
    public Timer timer = new Timer();
    public long between;

    public void CreateNewNote(String newMess, String day, String month,
    String year, String hour, String minute)
            throws Exception {
        dir.mkdir();
        String date = day + '_' + month + '_' + year + "__" + hour + '_' + minute;
        String path2 = path + date + ".txt";
        File file = new File("src/StakanPrjctOffline", date + ".txt");
        file.createNewFile();
        SimpleDateFormat format = new SimpleDateFormat("dd_M_yyyy__H_m");

        try (FileWriter writer = new FileWriter(path2, false);) {
            writer.write(newMess);
            writer.flush();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        note = newMess;
        try {
            Date datePath = format.parse(date);
            Timer(datePath, note);
        } catch (ParseException ex){
            System.out.println(ex.getMessage());
        }

    }

    public void DeleteOldNotes() throws NullPointerException {
        Date nowDate = new Date();
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd_M_yyyy__H_m");
            notes = dir.listFiles();
            String[] files = dir.list();
            if( files.length>0) {
                for (File i : notes) {
                    Date dateNote = format.parse(i.getName());
                    if (nowDate.after(dateNote)) {
                        i.delete();
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void SelectNote() throws IOException{
        try {
            notes = dir.listFiles();
            SimpleDateFormat format = new SimpleDateFormat("dd_M_yyyy__H_m");
            String[] files = dir.list();
            if( files.length>0) {
                for (File i : notes) {
                    Date dateNote = format.parse(i.getName());
                    String Message = readUsingBufferedReader(i.getName());
                    try {
                        Timer(dateNote, Message);
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static String readUsingBufferedReader(String fileName) throws IOException {
        BufferedReader reader = new BufferedReader( new FileReader (fileName));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");
        while( (line = reader.readLine()) != null ) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }

        stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }

    public void Timer(Date date, String Mes) throws Exception {
        Date nowDate = new Date();
        if (nowDate.before(date)) {
            between = date.getTime() - nowDate.getTime();
            TimerTask timerTask = new MyTimerTask(Mes);
            timer.schedule(timerTask, between);
        }
    }

    public class MyTimerTask extends TimerTask{
        public String STR;
        public MyTimerTask(String str)
        {
            STR = str;
        }
        public void run() {
            JOptionPane.showMessageDialog(null, STR);
        }
    }
}