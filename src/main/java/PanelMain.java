import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

public class PanelMain {
    private JLabel Clock;
    private JLabel WeatherIcon;
    private JButton addNote;
    private JButton Exit;
    private JButton VK;
    private JButton YouTube;
    public JPanel PanelMain;
    private JLabel TimeDay;
    private JLabel WeatherSky;
    private String Snowy = "Небольшой снег";

    public PanelMain() {
        YouTube.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().browse(URI.create("www.youtube.com/feed/subscriptions"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        Exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        VK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().browse(URI.create("www.vk.com/feed/subscriptions"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        addNote.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame FrameAdd = new JFrame ("Add Note");
                FrameAdd.setSize(new Dimension(330, 90));
                FrameAdd.setLocationRelativeTo(null);
                FrameAdd.setContentPane(new AddNote().AddNote);
                FrameAdd.setUndecorated(false);
                FrameAdd.setVisible(true);
                FrameAdd.setResizable(false);
            }
        });

        new Thread()
        {
            public void run()
            {
                int run = 0;
                while (run == 0) {
                    Calendar cal = new GregorianCalendar();

                    int hour = cal.get(Calendar.HOUR);
                    int min = cal.get(Calendar.MINUTE);
                    int sec = cal.get(Calendar.SECOND);
                    int T = cal.get(Calendar.AM_PM);

                    if (T == 1)
                        hour = hour + 12;

                    String time;

                    if (hour < 10)
                    {
                        time = "0" + hour + ":";
                        if (min < 10)
                        {
                            time = time + "0" + min + ":";
                            if (sec < 10)
                                time = time + "0" + sec;
                            else
                                time = time + sec;
                        }
                        else
                        {
                            time = time + min + ":";
                            if (sec < 10)
                                time = time + "0" + sec;
                            else
                                time = time + sec;
                        }
                    }
                    else
                    {
                        time = hour + ":";
                        if (min < 10)
                        {
                            time = time + "0" + min + ":";
                            if (sec < 10)
                                time = time + "0" + sec;
                            else
                                time = time + sec;
                        }
                        else
                        {
                            time = time + min + ":";
                            if (sec < 10)
                                time = time + "0" + sec;
                            else
                                time = time + sec;
                        }
                    }


                    Clock.setText(time);
                }
            }
        }.start();

        new Thread()
        {
            public void run()
            {
                ImageIcon iconLogo;
                WeatherParser Parser = new WeatherParser();
                Parser.WeatherParse();

                boolean Run = true;
                while (Run)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        ArrayList<Weather> Weathers = new ArrayList<Weather>(4);
                        Weathers = Parser.getWeathers();
                        Weather TemperatureAndSky = Weathers.get(i);
                        String PartOfDay = TemperatureAndSky.getDay_part_and_degree();
                        String Sky = TemperatureAndSky.getWeather();
                        TimeDay.setText(PartOfDay);
                        WeatherSky.setText(Sky);


                        if (Sky.equals("Небольшой снег") || Sky.equals("Дождь со снегом"))
                        {
                            iconLogo = new ImageIcon("src/WeatherPic/snowy1.jpg");
                            WeatherIcon.setIcon(iconLogo);
                        }
                        if (Sky.equals("Снег"))
                        {
                            iconLogo = new ImageIcon("src/WeatherPic/snowy2.jpg");
                            WeatherIcon.setIcon(iconLogo);
                        }
                        if (Sky.equals("Снегопад"))
                        {
                            iconLogo = new ImageIcon("src/WeatherPic/snowy3.jpg");
                            WeatherIcon.setIcon(iconLogo);
                        }
                        if (Sky.equals("Облачно"))
                        {
                            iconLogo = new ImageIcon("src/WeatherPic/cloudy.jpg");
                            WeatherIcon.setIcon(iconLogo);
                        }
                        if (Sky.equals("Пасмурно"))
                        {
                            iconLogo = new ImageIcon("src/WeatherPic/rainy.jpg");
                            WeatherIcon.setIcon(iconLogo);
                        }
                        if (Sky.equals("Солнечно"))
                        {
                            iconLogo = new ImageIcon("src/WeatherPic/suny.jpg");
                            WeatherIcon.setIcon(iconLogo);
                        }
                        try {
                            TimeUnit.SECONDS.sleep(4);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }.start();
    }
}
